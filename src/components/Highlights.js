import { Row, Col, Card } from 'react-bootstrap';


export default function Highlights() {
	return (

		<Row className="mt-3 mb-3">
			<Col xs={12} md={100}>
				<Card className="cardHighlight p-3">
				     <Card.Body>
				       <Card.Title>Friendly Sellers</Card.Title>
				       <Card.Text>
				       ShoeMart, guarantees friendly environment!
				       </Card.Text>
				     </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={100}>
				<Card className="cardHighlight p-3">
				     <Card.Body>
				       <Card.Title>Cash On Delivery</Card.Title>
				       <Card.Text>
				         No card? no Problem!
				       </Card.Text>
				     </Card.Body>
				</Card>

			</Col>

			<Col xs={12} md={100}>
				<Card className="cardHighlight p-3">
				     <Card.Body>
				       <Card.Title>Online Payment </Card.Title>
				       <Card.Text>
				        We also do online transactions!
				       </Card.Text>
				     </Card.Body>
				</Card>

			</Col>
		</Row>
		
			
	)
};
