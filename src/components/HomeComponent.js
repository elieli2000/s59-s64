import { useContext } from 'react';
import { Card, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Product({prodProp}) {

	const {user} = useContext(UserContext);


	return (

		<Container>
			<Card>
				<Card.Img variant="top" style={{height: '18rem'}} />
				<Card.Body>
				 	<Card.Title style={{ height: '3rem'}}>{prodProp.name}</Card.Title>
				 	<Card.Text style={{ height: '5rem'}}>{prodProp.desc}</Card.Text>
					 	<Card.Text>PHP: {prodProp.price } </Card.Text>
					 	{
					 		user === null || user.isAdmin === false
					 		?
					 		<Link className="btn btn-secondary" to={`/cart/${prodProp._id}`}>Add To Cart</Link>	
					 		:
					 		<Link className="btn btn-danger btn-block" to="/login">Login to Shop</Link>
					 	}
				</Card.Body>
			</Card>
		</Container>

	)



}