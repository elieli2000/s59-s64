import { useContext } from 'react';
import { Navbar, Nav, Container, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {

	const { user } = useContext(UserContext);

	return (


		<Navbar className="navbar navbar-expand-lg navbar-dark bg-black">
		     <Container fluid>
		       <Navbar.Brand as={Link} to="/">ShoeMart</Navbar.Brand>
		       <Navbar.Toggle aria-controls="navbarScroll" />
		       <Navbar.Collapse id="navbarScroll">
		         <Nav className="ml-auto">
		           <Nav.Link as={Link} to="/">Home</Nav.Link>
		           <Nav.Link as={Link} to="/viewProducts">Products</Nav.Link>
		           { user.id 
			       ?
			       		user.isAdmin
			       			?
			       			<>
			       			<Nav.Link as={Link} to="/viewAdmin">Admin Dashboard</Nav.Link>
			       			<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
			       			</>
			       			:
			       			<>
			       			<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
			       			</>
			       	:
			       	<>
			       		<Nav.Link as={Link} to="/login">Login</Nav.Link>
			       		<Nav.Link as={Link} to="/register">Register</Nav.Link>
			       	</>
			       }
		         </Nav>
		       </Navbar.Collapse>
		       
		     </Container>
		   </Navbar>
	)


	
}