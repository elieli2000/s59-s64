import {  useContext } from 'react';
import { Container, Card } from 'react-bootstrap';
import { Link, useNavigate, Navigate,} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Product({prodProp}) {

	const {user} = useContext(UserContext);
	const navigate = useNavigate();
	const { name, description, price, _id } = prodProp;

return (

	<Container>
		<Card className=" minCard text-center p-1 m-2">
		    <Card.Body>
		    	<Card.Title>{name}</Card.Title>
		    	<Card.Text>{description}</Card.Text>
		   	 	<Card.Text>PHP: {price }</Card.Text>
		   	 	{ user.id
		   	 	?
		   	 		user.isAdmin
		   	 			? 
		   	 			<>
		   	 	
		   	 			</>
		   	 			: 
		   	 			<>
		   	 				<Link className="btn btn-primary m-2" size="lg" to={`/create/${_id}`}>Buy</Link>	
		   	 			</>
		   	 	: <Link className="btn btn-warning btn-block" size="lg" to="/login">Login to Shop</Link>
		   	 	}
			</Card.Body>
		</Card>
	</Container>

	)

};