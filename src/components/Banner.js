import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Carousel, Container } from 'react-bootstrap';


export default function Banner({data}) {

	// console.log(data)
	const { title, content, destination, label } = data;

	return(
		<Row><center>
			<Col className="p-5">
				<h1>{title}</h1>
				<p>{content}</p>
				<Button variant="primary" as={Link} to={destination}>{label}</Button>
			</Col>
			</center>
		</Row>


		
	)
};
