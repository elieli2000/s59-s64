import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';

// Components
import AppNavbar from './components/AppNavbar';
import HomeComponent from './components/HomeComponent';

// Pages
import AdminDashboard from './pages/AdminDashboard'
import CreateProduct from './pages/CreateProduct';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout'
import Register from './pages/Register';
import RetrieveAllOrder from './pages/RetrieveAllOrder';
import CreateOrder from './pages/CreateOrder';

import UpdateProduct from './pages/UpdateProduct';

import ViewProducts from './pages/ViewProducts';
import './App.css';
import { UserProvider } from './UserContext';

function App() {

	const [user, setUser] = useState({
	    id: null,
	    isAdmin: null
	});

	const unsetUser = () => {
	  localStorage.clear()
	}

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}users/details`, {
		  headers: {
		    Authorization: `Bearer ${localStorage.getItem('token')}`
		  }
		})

	    .then(res => res.json())
	    .then(data => {

	      if(typeof data._id !== "undefined") {
	        setUser({
	          id: data._id,
	          isAdmin: data.isAdmin
	        })

	      } else {

	        setUser({
	          id: null,
	          isAdmin: null
	        })
	      }
	    })
	}, []);

  return (

  	<UserProvider value={{user, setUser, unsetUser}}>
	    <Router>
	    	<AppNavbar/>
	    		<Container>
					<Routes>
						<Route path="/viewAdmin" element={<AdminDashboard/>}/>
						<Route path="/" element={<Home/>} />
						<Route path="/retrieveAllOrder" element={<RetrieveAllOrder/>} />
						<Route path="/login" element={<Login/>} />
						<Route path="/logout" element={<Logout/>} />
						<Route path="/updateProduct/:productId" element= {<UpdateProduct/>}/>
						<Route path="/create/:productId" element={<CreateOrder/>} />
						<Route path="/register" element={<Register/>} />
					

						<Route path="/viewProducts" element= {<ViewProducts/>}/>
						<Route path="/createProduct" element= {<CreateProduct/>}/>	
						<Route path="/allOrders" element= {<RetrieveAllOrder/>}/>
					</Routes>
	    		</Container>
	    </Router>
    </UserProvider>
  );
}

export default App;
