import { useState, useEffect } from 'react';
import { Carousel, Container } from 'react-bootstrap';
import { Fragment } from 'react';
import HomeComponent from '../components/HomeComponent'

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


import '../App.css';

import React from 'react';

export default function Home() {

  const data = {
    title: "Welcome To ShoeMart",
    content: "Shoes Everywhere",
    destination: "/ViewProducts",
    label: "Shop Now!"
  } 

  return (
    <Fragment>
      <Banner data={data} />
      <Highlights/>
      {/*<CourseCard/>*/}
    </Fragment>
  )
};
