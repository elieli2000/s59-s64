import { useState, useEffect } from 'react';
import Product from '../components/Product'
import { Row, Col, Card } from 'react-bootstrap';

export default function ViewProduct() {

	const [prodDetails, setProductDetails]  = useState([])

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}products/`)
		.then(res => res.json())
		.then(data => {

			localStorage.getItem('token', data.access);

			setProductDetails(data.map(product => {
				return (
					<Product prodProp={product}/>
				)}
			))	
		})
		.catch(err =>{
			console.log(err)
			alert('error')
		})

	},[])



	return (
		<>
			<Col xs={40} md={100}>
				<Card className="cardHighlight my-5">
				     <Card.Body>
				      		<h1 className="my-5 text-center">Available Products</h1>
				     </Card.Body>

				</Card>
					</Col>
					<Col xs={4} md={12}>
				<Card className="cardHighlight my-5 mx-5">
				{prodDetails}	
				</Card>
				</Col>
		
	
			
			
		
		
		</>



	)
}