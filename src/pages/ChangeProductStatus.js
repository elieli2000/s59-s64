import { useState, useEffect, useContext} from 'react';
import { Card, Button, Container } from 'react-bootstrap';
import { useParams , useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'

export default function ChangeProductStatus () {

	const { prodId } = useParams();

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const [prodDetails,setProdDetails] = useState ({
		image: null,
		name: null,
		desc: null,
		price: null,
		isActive: null

	})

	let token = localStorage.getItem('token')

	useEffect(()=>{
	
		fetch(`${process.env.REACT_APP_API_URL}getSingleProduct/${prodId}`,{
			method: 'GET',
			headers:{
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			localStorage.getItem('token',data.access);
			console.log(data);
			if(data.message){

				Swal.fire({

					icon: "error",
					title: "Product Unavailable",
					text: data.message

				})

			} else {

				console.log("passed")
				setProdDetails({
					image: data.image,
					name: data.name,
					desc: data.desc,
					price: data.price,
					isActive: data.isActive
				})
				
			}
		})

	},[prodId])

	const activate = (prodId) => {
		
		fetch(`${process.env.REACT_APP_API_URL}products/activateProduct/${prodId}`,{

			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				prodId:prodId
			})
		})	
		.then(res => res.json())
		.then(data => {
			localStorage.getItem('token',data.access);
			setProdDetails({
			
				isActive: data.isActive
			})
			console.log(data)
		})
	}

	const deactivate = (prodId) => {

		fetch(`${process.env.REACT_APP_API_URL}products/deactivateProduct/${prodId}`, {
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				prodId:prodId
			})
		})	
		.then(res => res.json())
		.then(data => {
			localStorage.getItem('token', data.access);
			setProdDetails({
				
				isActive: data.isActive
			})
			console.log(data)
		})
	}

		return (
			<Container>
						<Card className="mt-3 mb-2 viCard" >
							<Card.Body className="text-center">
								<Card.Img variant="top" className="statusImg "src={prodDetails.image} />
								<Card.Title>{prodDetails.name}</Card.Title>
								<Card.Subtitle>Description:</Card.Subtitle>
								<Card.Text>{prodDetails.desc}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>{prodDetails.price}</Card.Text>
								{
									prodDetails.isActive
									?
									<Button variant="primary" block onClick={()=> deactivate(prodId)}>Deactivate</Button>
									:
									<Button variant="primary"  className="m-2" block onClick={()=> activate(prodId)}>Activate</Button>
								}
								<Button variant="primary" className="m-2" as={Link} to={`/update/${prodId}`}>Update</Button>
								
							</Card.Body>
						</Card>
			</Container>
			)



	}
