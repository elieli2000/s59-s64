import { useState, useEffect, useContext} from 'react';
import ProductCard from '../components/Product'
import { Table, Button } from 'react-bootstrap';
import { useParams, useNavigate, Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function RetrieveAllOrder(){

    const { user } = useContext(UserContext);

    const [ allOrders, setallOrders ] = useState([]);

    const fetchOrders = () => {

        fetch(`${process.env.REACT_APP_API_URL}orders/allorders`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setallOrders(data.map(order => {
                return (
                    <tr key={order._id}>
                        <td>{order._id}</td>
                        <td>{order.userId}</td>
                        <td>{order.totalAmount}</td>
                        <td>{order.purchasedOn}</td>
                    </tr>
                )
            }))
        })
    }


    useEffect(()=>{

        fetchOrders();
    }, [])

    return (
        (user.isAdmin)
                ?
                <>
                    <div className="mt-5 mb-3 text-center">
                        <h1>Admin Dashboard</h1>
             
                        <Button as={Link} to="/viewAdmin" variant="primary" size="lg" className="mx-2">View Products</Button>
                    </div>
                    <Table striped bordered hover>
                     <thead>
                       <tr>
                         <th>Order ID</th>
                         <th>User ID</th>
                         <th>Total Amount</th>
                         <th>Purchase Date</th>
                       </tr>
                     </thead>
                     <tbody>
                       { allOrders }
                     </tbody>
                   </Table>
                </>
                :
                <Navigate to="/viewProducts" />

    )

}

