import { useState, useEffect } from 'react';
import AdminProducts from '../components/adminProducts'
import { useParams, Link } from 'react-router-dom';

export default function AllProducts(){

	// const navigate = useNavigate();

	const [prodDetails, setProductDetails] = useState([])

	let token = localStorage.getItem('token')

	useEffect(() => {

		console.log(token)

		fetch(`${process.env.REACT_APP_API_URL}products/getAllProduct`,{
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			localStorage.getItem('token',data.accessToken);
			setProductDetails(data.map(product => {
				return (
					<AdminProducts prodProp={product}/>
				)
			}))	
		})
		.catch(error =>{
			console.log(error)
			alert('error')
		})
	},[])

	return (
		
		<>
			<h1 className="my-5 text-center">All Products</h1>
			{prodDetails}
		</>

		)
}
