import { Form, Button, Container } from 'react-bootstrap';
import { useNavigate, useParams } from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function UpdateProduct(){

	const{user} = useContext(UserContext);

	const { productId } = useParams();

	const navigate = useNavigate();

	const[ name, setName ] = useState("");
	const[ description, setDescription ] = useState("");
	const[ price, setPrice ] = useState(0);

	const[ isActive, setIsActive ] = useState(true);



	function updateProduct(e){

		e.preventDefault()
		console.log(productId)

		fetch(`${process.env.REACT_APP_API_URL}products/${productId}`,{

			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({

			name: name,
			description: description,
			price: price

			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data){
			

				Swal.fire({
					icon: "success",
					title: "Product Update Successful.",
					text: `product is now updated`
				});
				navigate(`/viewAdmin`);
				
				}else {
					Swal.fire({
					icon: "error",
					title: "Product Update Failed.",
					text: data.message
				});
			}

		})
		setName('');
		setDescription('');
		setPrice(0);

	};

	useEffect(()=>{

		if(name !== "" && description !== "" && price !== 0){
			
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[name, description, price]);

	return(
		<>
		<Container className="centerAdd">
			<h2 className = "text-center my-5 ">Update Product</h2>
			<Form onSubmit={ e => updateProduct(e)}>
				<Form.Group controlId="name">
					<Form.Label>Product Name:</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Enter Product Name"
						value={name}
						onChange={(e) => setName(e.target.value)}
						required
					/>
				</Form.Group>
				<Form.Group controlId="description">
					<Form.Label>Description:</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Enter Product Description"
						value={description}
						onChange={(e) => setDescription(e.target.value)}
						required
					/>
				</Form.Group>
				<Form.Group controlId="price">
					<Form.Label>Price:</Form.Label>
					<Form.Control 
						type="number"
						placeholder="Enter Price"
						value={price}
						onChange={(e) => setPrice(e.target.value)}
						required
					/>
				</Form.Group>								

				{
					isActive 
					? <Button type="submit" variant="primary" className = "btnCrtProd">Submit</Button>
					: <Button type="submit" variant="danger" disabled className = "btnCrtProd">Submit</Button>
				}
			</Form>
			</Container>
		</>
	)
}
